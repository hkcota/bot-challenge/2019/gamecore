package game_test

import (
	"testing"

	"gitlab.com/hkcota/bot-challenge/pkg/engine"
	"gitlab.com/hkcota/bot-challenge/pkg/game"
)

func TestBombPlacementBomb_valid(t *testing.T) {
	currentState := engine.Frame{
		ID: 1,
		Objects: []engine.Object{
			{
				ID:     "agent/1",
				Type:   "agent",
				Player: 1,
				Position: engine.Position{
					X: 1,
					Y: 1,
				},
			},
			{
				ID:     "agent/2",
				Type:   "agent",
				Player: 2,
				Position: engine.Position{
					X: 2,
					Y: 2,
				},
			},
		},
	}
	commands := []engine.Command{
		{
			Player: 1,
			Agent:  "agent/1",
			Verb:   "place",
			Target: "bomb",
		},
	}

	nextState, err := game.
		BombPlacementRule().
		Resolve(currentState, commands)
	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}

	forks := engine.
		From(nextState.Objects).
		Fork(
			engine.ObjectHasID("agent/1"),
			engine.ObjectIsType("bomb"),
		)

	agent1, bombs := forks[0].All()[0], forks[1].All()
	if want, have := 1, len(bombs); want != have {
		t.Errorf("expected %d bomb in the slice, got %d", want, have)
		t.FailNow()
	}
	if want, have := agent1.Position, bombs[0].Position; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func TestBombPlacementBomb_commandConflict(t *testing.T) {
	currentState := engine.Frame{
		ID: 1,
		Objects: []engine.Object{
			{
				ID:     "agent/1",
				Type:   "agent",
				Player: 1,
				Position: engine.Position{
					X: 1,
					Y: 1,
				},
			},
			{
				ID:     "agent/2",
				Type:   "agent",
				Player: 2,
				Position: engine.Position{
					X: 2,
					Y: 2,
				},
			},
		},
	}
	commands := []engine.Command{
		{
			Player: 1,
			Agent:  "agent/1",
			Verb:   "place",
			Target: "bomb",
		},
		{
			Player: 1,
			Agent:  "agent/1",
			Verb:   "place",
			Target: "bomb",
		},
	}

	_, err := game.
		BombPlacementRule().
		Resolve(currentState, commands)
	if err == nil {
		t.Errorf("expected error but got nil")
	} else if terr, ok := err.(*engine.CommandConflict); !ok {
		t.Errorf("expected *engine.CommandConflict, got %T", err)
	} else if want, have := commands[0], terr.Command1(); want != have {
		t.Errorf("\nexpected: %s\n     got: %s", want, have)
	} else if want, have := commands[1], terr.Command2(); want != have {
		t.Errorf("\nexpected: %s\n     got: %s", want, have)
	} else if want, have := "cannot issue 2 commands to the same agent", terr.Msg(); want != have {
		t.Errorf("\nexpected: %s\n     got: %s", want, have)
	}
}
func TestExplosionRule_chainBomb(t *testing.T) {
	/*
		# currentState
		┌─┬─┬─┬─┬─┬─┐
		│1│ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┤
		│2│ │4│ │ │ │
		├─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┤
		│ │ │ │ │3│ │
		├─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │w│
		└─┴─┴─┴─┴─┴─┘

		# expected nextState
		┌─┬─┬─┬─┬─┬─┐
		│x│ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┤
		│x│ │x│ │ │ │
		├─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┤
		│ │ │ │ │3│ │
		├─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │w│
		└─┴─┴─┴─┴─┴─┘
	*/
	currentState := engine.Frame{
		ID: 1,
		Objects: []engine.Object{
			{
				ID:     "bomb/1",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 1,
					Y: 1,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 1,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
			{
				ID:     "bomb/2",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 1,
					Y: 3,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
			{
				ID:     "wall/1",
				Type:   "wall",
				Player: 1,
				Position: engine.Position{
					X: 6,
					Y: 6,
				},
			},
			{
				ID:     "bomb/3",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 5,
					Y: 5,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
			{
				ID:     "bomb/4",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 3,
					Y: 3,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
		},
	}
	nextState, err := game.ExplosionRule().Resolve(currentState, []engine.Command{})
	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}

	var vals []engine.Object
	forks := engine.From(nextState.Objects).Fork(
		engine.ObjectHasID("bomb/1"),
		engine.ObjectHasID("bomb/2"),
		engine.ObjectHasID("bomb/3"),
		engine.ObjectHasID("bomb/4"),
	)

	vals = forks[0].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "exploding", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[1].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "exploding", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[2].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "will-explode", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[3].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "exploding", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func TestExplosionRule_bombWithWall(t *testing.T) {
	/*
		# currentState
		┌─┬─┬─┐
		│1│w│2│
		├─┼─┼─┤
		│ │ │ │
		├─┼─┼─┤
		│3│ │ │
		└─┴─┴─┘

		# expected nextState
		┌─┬─┬─┐
		│x│x│2│
		├─┼─┼─┤
		│ │ │ │
		├─┼─┼─┤
		│x│ │ │
		└─┴─┴─┘
	*/
	currentState := engine.Frame{
		ID: 1,
		Objects: []engine.Object{
			{
				ID:     "bomb/1",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 1,
					Y: 1,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 1,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
			{
				ID:     "bomb/2",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 3,
					Y: 1,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
			{
				ID:     "bomb/3",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 1,
					Y: 3,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
			{
				ID:     "wall/1",
				Type:   "wall",
				Player: 0,
				Position: engine.Position{
					X: 2,
					Y: 1,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
		},
	}

	nextState, err := game.ExplosionRule().Resolve(currentState, []engine.Command{})
	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}

	var vals []engine.Object
	forks := engine.From(nextState.Objects).Fork(
		engine.ObjectHasID("bomb/1"),
		engine.ObjectHasID("bomb/2"),
		engine.ObjectHasID("bomb/3"),
		engine.ObjectHasID("wall/1"),
	)

	vals = forks[0].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "exploding", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[1].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "will-explode", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[2].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "exploding", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[3].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "destroying", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func TestExplosionRule_explosionPowerWithWalls(t *testing.T) {
	/*
		# currentState
		┌─┬─┬─┬─┬─┬─┬─┐
		│ │ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │w│ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │w│b│ │ │w│ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │w│ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │ │
		└─┴─┴─┴─┴─┴─┴─┘

		# expected nextState
		Note: expected explosion power of b
		 * top:    4
		 * right:  3
		 * bottom: 2
		 * left:   1
		┌─┬─┬─┬─┬─┬─┬─┐
		│ │ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │x│ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │x│b│ │ │x│ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │x│ │ │ │ │
		├─┼─┼─┼─┼─┼─┼─┤
		│ │ │ │ │ │ │ │
		└─┴─┴─┴─┴─┴─┴─┘
	*/
	currentState := engine.Frame{
		ID: 1,
		Objects: []engine.Object{
			{
				ID:     "bomb/1",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 3,
					Y: 6,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 1,
				},
				AttrsInt: map[string]int{
					"Power": 10, // more than enough
				},
			},
			{
				ID:     "wall/1",
				Type:   "wall",
				Player: 0,
				Position: engine.Position{
					X: 3,
					Y: 2,
				},
			},
			{
				ID:     "wall/2",
				Type:   "wall",
				Player: 0,
				Position: engine.Position{
					X: 6,
					Y: 6,
				},
			},
			{
				ID:     "wall/3",
				Type:   "wall",
				Player: 0,
				Position: engine.Position{
					X: 3,
					Y: 8,
				},
			},
			{
				ID:     "wall/4",
				Type:   "wall",
				Player: 0,
				Position: engine.Position{
					X: 2,
					Y: 6,
				},
			},
		},
	}

	nextState, err := game.ExplosionRule().Resolve(currentState, []engine.Command{})
	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}

	var vals []engine.Object
	forks := engine.From(nextState.Objects).Fork(
		engine.ObjectHasID("bomb/1"),
	)

	vals = forks[0].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
		t.FailNow()
	}

	bomb := vals[0]
	if want, have := "exploding", bomb.State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if want, have := 4, bomb.AttrInt("Explosion:up"); want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
	if want, have := 3, bomb.AttrInt("Explosion:right"); want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
	if want, have := 2, bomb.AttrInt("Explosion:down"); want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
	if want, have := 1, bomb.AttrInt("Explosion:left"); want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
}

func TestExplosionRule_explosionPowerWithBombs(t *testing.T) {
	/*
		# currentState
		┌──┬──┬──┬──┬──┬──┬──┐
		│  │  │  │  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │b2│  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │  │  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │  │  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │  │  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │b5│b1│  │  │b3│  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │  │  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │b4│  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │  │  │  │  │  │
		└──┴──┴──┴──┴──┴──┴──┘

		# expected nextState
		Note: expected explosion power of b1
		 * top:    4
		 * right:  3
		 * bottom: 2
		 * left:   1
		┌──┬──┬──┬──┬──┬──┬──┐
		│  │  │  │  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │x2│  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │  │  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │  │  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │  │  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │x5│x1│  │  │x3│  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │  │  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │x4│  │  │  │  │
		├──┼──┼──┼──┼──┼──┼──┤
		│  │  │  │  │  │  │  │
		└──┴──┴──┴──┴──┴──┴──┘
	*/
	currentState := engine.Frame{
		ID: 1,
		Objects: []engine.Object{
			{
				ID:     "bomb/1",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 3,
					Y: 6,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 1,
				},
				AttrsInt: map[string]int{
					"Power": 10, // more than enough
				},
			},
			{
				ID:     "bomb/2",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 3,
					Y: 2,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 1, // does not matter
				},
			},
			{
				ID:     "bomb/3",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 6,
					Y: 6,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 1, // does not matter
				},
			},
			{
				ID:     "bomb/4",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 3,
					Y: 8,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 1, // does not matter
				},
			},
			{
				ID:     "bomb/5",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 2,
					Y: 6,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 1, // does not matter
				},
			},
		},
	}

	nextState, err := game.ExplosionRule().Resolve(currentState, []engine.Command{})
	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}

	var vals []engine.Object
	forks := engine.From(nextState.Objects).Fork(
		engine.ObjectHasID("bomb/1"),
	)

	vals = forks[0].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
		t.FailNow()
	}

	bomb := vals[0]
	if want, have := "exploding", bomb.State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if want, have := 4, bomb.AttrInt("Explosion:up"); want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
	if want, have := 3, bomb.AttrInt("Explosion:right"); want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
	if want, have := 2, bomb.AttrInt("Explosion:down"); want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
	if want, have := 1, bomb.AttrInt("Explosion:left"); want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
}
func TestExplosionRule_bombPowerDifference(t *testing.T) {
	/*
		# currentState
		# (1) b1 has the power 2, and will explode.
		# (2) b2 has power 3.
		# (2) b3 has power 1.
		┌──┬──┬──┬──┐
		│b1│  │  │b2│
		├──┼──┼──┼──┤
		│  │  │  │  │
		├──┼──┼──┼──┤
		│b3│  │  │  │
		└──┴──┴──┴──┘

		# expected nextState
		┌──┬──┬──┬──┐
		│bx│  │  │b2│
		├──┼──┼──┼──┤
		│  │  │  │  │
		├──┼──┼──┼──┤
		│bx│  │  │  │
		└──┴──┴──┴──┘
	*/

	currentState := engine.Frame{
		ID: 1,
		Objects: []engine.Object{
			{
				ID:     "bomb/1",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 1,
					Y: 1,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 1,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
			{
				ID:     "bomb/2",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 4,
					Y: 1,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 1,
				},
			},
			{
				ID:     "bomb/3",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 1,
					Y: 3,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 3,
				},
			},
		},
	}

	nextState, err := game.ExplosionRule().Resolve(currentState, []engine.Command{})
	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}

	var vals []engine.Object
	forks := engine.From(nextState.Objects).Fork(
		engine.ObjectHasID("bomb/1"),
		engine.ObjectHasID("bomb/2"),
		engine.ObjectHasID("bomb/3"),
	)

	vals = forks[0].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "exploding", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[1].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "will-explode", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[2].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "exploding", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func TestExplosionRule_bombWithWallAgent(t *testing.T) {
	/*
		# currentState
		┌──┬──┬──┐
		│b1│w1│b2│
		├──┼──┼──┤
		│  │  │a2│
		├──┼──┼──┤
		│b3│  │a1│
		└──┴──┴──┘

		# expected nextState
		┌──┬──┬──┐
		│bx│wx│b2│
		├──┼──┼──┤
		│  │  │a2│
		├──┼──┼──┤
		│bx│  │ax│
		└──┴──┴──┘
	*/
	currentState := engine.Frame{
		ID: 1,
		Objects: []engine.Object{
			{
				ID:     "bomb/1",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 1,
					Y: 1,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 1,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
			{
				ID:     "bomb/2",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 3,
					Y: 1,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
			{
				ID:     "bomb/3",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 1,
					Y: 3,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
			{
				ID:     "wall/1",
				Type:   "wall",
				Player: 0,
				Position: engine.Position{
					X: 2,
					Y: 1,
				},
				AttrsInt: map[string]int{},
			},
			{
				ID:     "agent/1",
				Type:   "agent",
				Player: 0,
				Position: engine.Position{
					X: 3,
					Y: 3,
				},
			},
			{
				ID:     "agent/2",
				Type:   "agent",
				Player: 0,
				Position: engine.Position{
					X: 3,
					Y: 2,
				},
			},
		},
	}

	nextState, err := game.ExplosionRule().Resolve(currentState, []engine.Command{})
	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}

	var vals []engine.Object
	forks := engine.From(nextState.Objects).Fork(
		engine.ObjectHasID("bomb/1"),
		engine.ObjectHasID("bomb/2"),
		engine.ObjectHasID("bomb/3"),
		engine.ObjectHasID("wall/1"),
		engine.ObjectHasID("agent/1"),
		engine.ObjectHasID("agent/2"),
	)

	vals = forks[0].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "exploding", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[1].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "will-explode", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[2].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "exploding", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[3].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "destroying", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[4].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "dying", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[5].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func TestExplosionRule_agentOnBomb(t *testing.T) {
	/*
		# currentState
		┌───┐
		│b+a│
		└───┘

		# expected nextState
		┌───┐
		│x+x│
		└───┘
	*/

	currentState := engine.Frame{
		ID: 1,
		Objects: []engine.Object{
			{
				ID:     "bomb/1",
				Type:   "bomb",
				Player: 0,
				Position: engine.Position{
					X: 1,
					Y: 1,
				},
				State: engine.State{
					Name:      "will-explode",
					CountDown: 1,
				},
				AttrsInt: map[string]int{
					"Power": 2,
				},
			},
			{
				ID:     "agent/1",
				Type:   "agent",
				Player: 1,
				Position: engine.Position{
					X: 1,
					Y: 1,
				},
			},
		},
	}

	nextState, err := game.ExplosionRule().Resolve(currentState, []engine.Command{})
	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}

	var vals []engine.Object
	forks := engine.From(nextState.Objects).Fork(
		engine.ObjectHasID("bomb/1"),
		engine.ObjectHasID("agent/1"),
	)

	vals = forks[0].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "exploding", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	vals = forks[1].All()
	if want, have := 1, len(vals); want != have {
		t.Errorf("expected %d, got %d", want, have)
	} else if want, have := "dying", vals[0].State.Name; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func getPercentageKey(direction engine.GridDirection) (percentageKey string) {
	switch direction {
	case engine.Rightward:
		percentageKey = "Percentage:x"
	case engine.Leftward:
		percentageKey = "Percentage:x"
	case engine.Upward:
		percentageKey = "Percentage:y"
	case engine.Downward:
		percentageKey = "Percentage:y"
	default:
		panic("unknown direction")
	}
	return
}

func TestMovementRule_movementCommand(t *testing.T) {
	makeState := func(pos engine.Position, speed, percentageX, percentageY int, direction engine.GridDirection) engine.Frame {
		return engine.Frame{
			ID: 1,
			Objects: []engine.Object{
				{
					ID:       "agent/1",
					Type:     "agent",
					Player:   1,
					Position: pos,
					State: engine.State{
						Name: "moving",
					},
					AttrsInt: map[string]int{
						// speed in percentage
						// means "percentage of cell per frame"
						"Speed":        speed,
						"Percentage:x": percentageX,
						"Percentage:y": percentageY,
						"Direction":    int(direction),
					},
				},
			},
		}
	}
	tests := []struct {
		what     string
		expected string
		speed    int
		command  engine.Command

		currentDirection   engine.GridDirection
		currentPos         engine.Position
		currentPercentageX int
		currentPercentageY int

		expectedState       string
		expectedDirection   engine.GridDirection
		expectedPos         engine.Position
		expectedPercentageX int
		expectedPercentageY int
	}{
		{
			what:     "move down when moving right, within speed",
			expected: "able to move down",
			speed:    9,
			command: engine.Command{
				Player: 1,
				Agent:  "agent/1",
				Verb:   "move",
				Target: "down",
			},

			currentDirection:   engine.Rightward,
			currentPos:         engine.Position{X: 2, Y: 2},
			currentPercentageX: -8,
			currentPercentageY: 0,

			expectedState:       "moving",
			expectedDirection:   engine.Downward,
			expectedPos:         engine.Position{X: 2, Y: 2},
			expectedPercentageX: 0,
			expectedPercentageY: 9,
		},
		{
			what:     "move down when moving right, exceeded speed",
			expected: "unable to move down",
			speed:    9,
			command: engine.Command{
				Player: 1,
				Agent:  "agent/1",
				Verb:   "move",
				Target: "down",
			},

			currentDirection:   engine.Rightward,
			currentPos:         engine.Position{X: 2, Y: 2},
			currentPercentageX: -9,
			currentPercentageY: 0,

			expectedState:       "stop",
			expectedDirection:   engine.Downward,
			expectedPos:         engine.Position{X: 2, Y: 2},
			expectedPercentageX: -9,
			expectedPercentageY: 0,
		},
		{
			what:     "move down when moving left, within speed",
			expected: "able to move down",
			speed:    9,
			command: engine.Command{
				Player: 1,
				Agent:  "agent/1",
				Verb:   "move",
				Target: "down",
			},

			currentDirection:   engine.Leftward,
			currentPos:         engine.Position{X: 2, Y: 2},
			currentPercentageX: 8,
			currentPercentageY: 0,

			expectedState:       "moving",
			expectedDirection:   engine.Downward,
			expectedPos:         engine.Position{X: 2, Y: 2},
			expectedPercentageX: 0,
			expectedPercentageY: 9,
		},
		{
			what:     "move down when moving left, exceeded speed",
			expected: "unable to move down",
			speed:    9,
			command: engine.Command{
				Player: 1,
				Agent:  "agent/1",
				Verb:   "move",
				Target: "down",
			},

			currentDirection:   engine.Leftward,
			currentPos:         engine.Position{X: 2, Y: 2},
			currentPercentageX: 9,
			currentPercentageY: 0,

			expectedState:       "stop",
			expectedDirection:   engine.Downward,
			expectedPos:         engine.Position{X: 2, Y: 2},
			expectedPercentageX: 9,
			expectedPercentageY: 0,
		},
		{
			what:     "move right when moving down, within speed",
			expected: "able to move right",
			speed:    9,
			command: engine.Command{
				Player: 1,
				Agent:  "agent/1",
				Verb:   "move",
				Target: "right",
			},

			currentDirection:   engine.Downward,
			currentPos:         engine.Position{X: 2, Y: 2},
			currentPercentageX: 0,
			currentPercentageY: -8,

			expectedState:       "moving",
			expectedDirection:   engine.Rightward,
			expectedPos:         engine.Position{X: 2, Y: 2},
			expectedPercentageX: 9,
			expectedPercentageY: 0,
		},
		{
			what:     "move right when moving down, exceeded speed",
			expected: "unable to move right",
			speed:    9,
			command: engine.Command{
				Player: 1,
				Agent:  "agent/1",
				Verb:   "move",
				Target: "right",
			},

			currentDirection:   engine.Downward,
			currentPos:         engine.Position{X: 2, Y: 2},
			currentPercentageX: 0,
			currentPercentageY: -9,

			expectedState:       "stop",
			expectedDirection:   engine.Rightward,
			expectedPos:         engine.Position{X: 2, Y: 2},
			expectedPercentageX: 0,
			expectedPercentageY: -9,
		},
		{
			what:     "move right when moving up, within speed",
			expected: "able to move right",
			speed:    9,
			command: engine.Command{
				Player: 1,
				Agent:  "agent/1",
				Verb:   "move",
				Target: "right",
			},

			currentDirection:   engine.Upward,
			currentPos:         engine.Position{X: 2, Y: 2},
			currentPercentageX: 0,
			currentPercentageY: 8,

			expectedState:       "moving",
			expectedDirection:   engine.Rightward,
			expectedPos:         engine.Position{X: 2, Y: 2},
			expectedPercentageX: 9,
			expectedPercentageY: 0,
		},
		{
			what:     "move right when moving up, exceeded speed",
			expected: "unable to move right",
			speed:    9,
			command: engine.Command{
				Player: 1,
				Agent:  "agent/1",
				Verb:   "move",
				Target: "right",
			},

			currentDirection:   engine.Upward,
			currentPos:         engine.Position{X: 2, Y: 2},
			currentPercentageX: 0,
			currentPercentageY: 9,

			expectedState:       "stop",
			expectedDirection:   engine.Rightward,
			expectedPos:         engine.Position{X: 2, Y: 2},
			expectedPercentageX: 0,
			expectedPercentageY: 9,
		},
		{
			what:     "issuing command ot another player's agent",
			expected: "no effect",
			speed:    9,
			command: engine.Command{
				Player: 2,
				Agent:  "agent/1",
				Verb:   "move",
				Target: "down",
			},

			currentDirection:   engine.Rightward,
			currentPos:         engine.Position{X: 2, Y: 2},
			currentPercentageX: -8,
			currentPercentageY: 0,

			expectedState:       "moving",
			expectedDirection:   engine.Rightward,
			expectedPos:         engine.Position{X: 2, Y: 2},
			expectedPercentageX: 1,
			expectedPercentageY: 0,
		},
	}

	var err error
	for _, test := range tests {
		currentState := makeState(
			test.currentPos,
			test.speed,
			test.currentPercentageX,
			test.currentPercentageY,
			test.currentDirection,
		)

		currentState, err = game.MovementRule(20, 20).Resolve(
			currentState,
			[]engine.Command{test.command},
		)
		if err != nil {
			t.Errorf("\n     what: %s\nexpecting: %s\nunexpected error: %s",
				test.what, test.expected, err)
		}

		agent := engine.
			From(currentState.Objects).
			Where(engine.ObjectIsType("agent")).
			All()[0]

		finalPos, finalState := agent.Position, agent.State.Name

		if want, have := test.expectedPos, finalPos; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
		if want, have := test.expectedPercentageX, agent.AttrInt("Percentage:x"); want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
		if want, have := test.expectedPercentageY, agent.AttrInt("Percentage:y"); want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
		if want, have := test.expectedState, finalState; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
	}
}

func TestMovementRule_simpleMovement(t *testing.T) {
	makeState := func(pos engine.Position, speed, percentage int, direction engine.GridDirection) engine.Frame {
		percentageKey := getPercentageKey(direction)
		return engine.Frame{
			ID: 1,
			Objects: []engine.Object{
				{
					ID:       "agent/1",
					Type:     "agent",
					Player:   0,
					Position: pos,
					State: engine.State{
						Name: "moving",
					},
					AttrsInt: map[string]int{
						// speed in percentage
						// means "percentage of cell per frame"
						"Speed":       speed,
						percentageKey: percentage,
						"Direction":   int(direction),
					},
				},
			},
		}
	}
	tests := []struct {
		what               string
		expected           string
		speed              int
		direction          engine.GridDirection
		framesToWait       int
		currentPos         engine.Position
		currentPercentage  int
		expectedPos        engine.Position
		expectedPercentage int
	}{
		{
			what:               "9% per frame for 5 frame",
			expected:           "only percentage change",
			speed:              9,
			direction:          engine.Rightward,
			framesToWait:       5,
			currentPos:         engine.Position{X: 1, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 1, Y: 1},
			expectedPercentage: 45,
		},
		{
			what:               "9% per frame for 6 frame",
			expected:           "1 step and negative percentage",
			speed:              9,
			direction:          engine.Rightward,
			framesToWait:       6,
			currentPos:         engine.Position{X: 1, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 2, Y: 1},
			expectedPercentage: -46,
		},
		{
			what:               "9% per frame for 12 frame",
			expected:           "1 step and positive percentage",
			speed:              9,
			direction:          engine.Rightward,
			framesToWait:       12,
			currentPos:         engine.Position{X: 1, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 2, Y: 1},
			expectedPercentage: 8,
		},
		{
			what:               "9% per frame for 5 frame, leftward",
			expected:           "only percentage change",
			speed:              9,
			direction:          engine.Leftward,
			framesToWait:       5,
			currentPos:         engine.Position{X: 10, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 10, Y: 1},
			expectedPercentage: -45,
		},
		{
			what:               "9% per frame for 6 frame, leftward",
			expected:           "1 step and negative percentage",
			speed:              9,
			direction:          engine.Leftward,
			framesToWait:       6,
			currentPos:         engine.Position{X: 10, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 9, Y: 1},
			expectedPercentage: 46,
		},
		{
			what:               "9% per frame for 12 frame, leftward",
			expected:           "1 step and positive percentage",
			speed:              9,
			direction:          engine.Leftward,
			framesToWait:       12,
			currentPos:         engine.Position{X: 10, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 9, Y: 1},
			expectedPercentage: -8,
		},
		{
			what:               "10% per frame for 90 frame",
			expected:           "just arrive at the 10th cell (1 + 9)",
			speed:              10,
			direction:          engine.Rightward,
			framesToWait:       90,
			currentPos:         engine.Position{X: 1, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 10, Y: 1},
			expectedPercentage: 0,
		},
		{
			what:               "10% per frame for 90 frame, downward",
			expected:           "just arrive at the 10th cell (1 + 9)",
			speed:              10,
			direction:          engine.Downward,
			framesToWait:       90,
			currentPos:         engine.Position{X: 1, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 1, Y: 10},
			expectedPercentage: 0,
		},
		{
			what:               "10% per frame for 90 frame, leftward",
			expected:           "just arrive at the 10th cell (1 + 9)",
			speed:              10,
			direction:          engine.Leftward,
			framesToWait:       90,
			currentPos:         engine.Position{X: 10, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 1, Y: 1},
			expectedPercentage: 0,
		},
		{
			what:               "10% per frame for 90 frame, upward",
			expected:           "just arrive at the 10th cell (1 + 9)",
			speed:              10,
			direction:          engine.Upward,
			framesToWait:       90,
			currentPos:         engine.Position{X: 1, Y: 10},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 1, Y: 1},
			expectedPercentage: 0,
		},
	}

	var err error
	for _, test := range tests {
		currentState := makeState(
			test.currentPos,
			test.speed,
			test.currentPercentage,
			test.direction,
		)

		for i := 0; i < test.framesToWait; i++ {
			currentState, err = game.MovementRule(20, 20).Resolve(
				currentState,
				[]engine.Command{},
			)
			if err != nil {
				t.Errorf("\n     what: %s\nexpecting: %s\nunexpected error on step %d: %s",
					test.what, test.expected, i, err)
			}
		}

		finalPos, finalPercentage :=
			currentState.Objects[0].Position,
			currentState.Objects[0].AttrInt(getPercentageKey(test.direction))

		if want, have := test.expectedPos, finalPos; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
		if want, have := test.expectedPercentage, finalPercentage; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
	}
}

func TestMovementRule_gridBoundary(t *testing.T) {
	makeState := func(pos engine.Position, speed, percentage int, direction engine.GridDirection) engine.Frame {
		percentageKey := getPercentageKey(direction)
		return engine.Frame{
			ID: 1,
			Objects: []engine.Object{
				{
					ID:       "agent/1",
					Type:     "agent",
					Player:   0,
					Position: pos,
					State: engine.State{
						Name: "moving",
					},
					AttrsInt: map[string]int{
						// speed in percentage
						// means "percentage of cell per frame"
						"Speed":       speed,
						percentageKey: percentage,
						"Direction":   int(direction),
					},
				},
			},
		}
	}
	tests := []struct {
		what               string
		expected           string
		speed              int
		direction          engine.GridDirection
		framesToWait       int
		currentPos         engine.Position
		currentPercentage  int
		expectedPos        engine.Position
		expectedPercentage int
		expectedState      string
	}{
		{
			what:               "10% per frame for 11 frames to right boundary",
			expected:           "stop by right boundary",
			speed:              10,
			direction:          engine.Rightward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 19, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 20, Y: 1},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "201% per frame for 1 frames to right boundary",
			expected:           "stop by right boundary",
			speed:              201,
			direction:          engine.Rightward,
			framesToWait:       1,
			currentPos:         engine.Position{X: 19, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 20, Y: 1},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "10% per frame for 11 frames to left boundary",
			expected:           "stop by left boundary",
			speed:              10,
			direction:          engine.Leftward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 2, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 1, Y: 1},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "201% per frame for 1 frames to left boundary",
			expected:           "stop by left boundary",
			speed:              201,
			direction:          engine.Leftward,
			framesToWait:       1,
			currentPos:         engine.Position{X: 2, Y: 1},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 1, Y: 1},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "10% per frame for 11 frames to top boundary",
			expected:           "stop by top boundary",
			speed:              10,
			direction:          engine.Upward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 1, Y: 2},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 1, Y: 1},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "201% per frame for 1 frames to top boundary",
			expected:           "stop by top boundary",
			speed:              201,
			direction:          engine.Upward,
			framesToWait:       1,
			currentPos:         engine.Position{X: 1, Y: 2},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 1, Y: 1},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "10% per frame for 11 frames to bottom boundary",
			expected:           "stop by bottom boundary",
			speed:              10,
			direction:          engine.Downward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 1, Y: 19},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 1, Y: 20},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "201% per frame for 1 frames to bottom boundary",
			expected:           "stop by bottom boundary",
			speed:              201,
			direction:          engine.Downward,
			framesToWait:       1,
			currentPos:         engine.Position{X: 1, Y: 19},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 1, Y: 20},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
	}

	var err error
	for _, test := range tests {
		currentState := makeState(
			test.currentPos,
			test.speed,
			test.currentPercentage,
			test.direction,
		)

		for i := 0; i < test.framesToWait; i++ {
			currentState, err = game.MovementRule(20, 20).Resolve(
				currentState,
				[]engine.Command{},
			)
			if err != nil {
				t.Errorf("\n     what: %s\nexpecting: %s\nunexpected error on step %d: %s",
					test.what, test.expected, i, err)
			}
		}

		finalPos, finalPercentage, finalState :=
			currentState.Objects[0].Position,
			currentState.Objects[0].AttrInt(getPercentageKey(test.direction)),
			currentState.Objects[0].State.Name

		if want, have := test.expectedPos, finalPos; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
		if want, have := test.expectedPercentage, finalPercentage; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
		if want, have := test.expectedState, finalState; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
	}
}

func TestMovementRule_stopByWall(t *testing.T) {
	makeState := func(pos engine.Position, speed, percentage int, direction engine.GridDirection) engine.Frame {
		percentageKey := getPercentageKey(direction)
		return engine.Frame{
			ID: 1,
			Objects: []engine.Object{
				{
					ID:     "wall/1",
					Type:   "wall",
					Player: 1,
					Position: engine.Position{
						X: 5,
						Y: 5,
					},
				},
				{
					ID:       "agent/1",
					Type:     "agent",
					Player:   0,
					Position: pos,
					State: engine.State{
						Name: "moving",
					},
					AttrsInt: map[string]int{
						// speed in percentage
						// means "percentage of cell per frame"
						"Speed":       speed,
						percentageKey: percentage,
						"Direction":   int(direction),
					},
				},
			},
		}
	}
	tests := []struct {
		what               string
		expected           string
		speed              int
		direction          engine.GridDirection
		framesToWait       int
		currentPos         engine.Position
		currentPercentage  int
		expectedPos        engine.Position
		expectedPercentage int
		expectedState      string
	}{
		{
			what:               "10% per frame for 11 frames right into the wall",
			expected:           "stop by the wall",
			speed:              10,
			direction:          engine.Rightward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 3, Y: 5},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 4, Y: 5},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "10% per frame for 11 frames left into the wall",
			expected:           "stop by the wall",
			speed:              10,
			direction:          engine.Leftward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 7, Y: 5},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 6, Y: 5},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "10% per frame for 11 frames down into the wall",
			expected:           "stop by the wall",
			speed:              10,
			direction:          engine.Downward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 5, Y: 3},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 5, Y: 4},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "10% per frame for 11 frames up into the wall",
			expected:           "stop by the wall",
			speed:              10,
			direction:          engine.Upward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 5, Y: 7},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 5, Y: 6},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
	}

	var err error
	for _, test := range tests {
		currentState := makeState(
			test.currentPos,
			test.speed,
			test.currentPercentage,
			test.direction,
		)

		for i := 0; i < test.framesToWait; i++ {
			currentState, err = game.MovementRule(20, 20).Resolve(
				currentState,
				[]engine.Command{},
			)
			if err != nil {
				t.Errorf("\n     what: %s\nexpecting: %s\nunexpected error on step %d: %s",
					test.what, test.expected, i, err)
			}
		}

		agent := engine.
			From(currentState.Objects).
			Where(engine.ObjectIsType("agent")).
			All()[0]

		finalPos, finalPercentage, finalState :=
			agent.Position,
			agent.AttrInt(getPercentageKey(test.direction)),
			agent.State.Name

		if want, have := test.expectedPos, finalPos; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
		if want, have := test.expectedPercentage, finalPercentage; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
		if want, have := test.expectedState, finalState; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
	}
}

func TestMovementRule_stopByBomb(t *testing.T) {
	makeState := func(pos engine.Position, speed, percentage int, direction engine.GridDirection) engine.Frame {
		percentageKey := getPercentageKey(direction)
		return engine.Frame{
			ID: 1,
			Objects: []engine.Object{
				{
					ID:     "bomb/1",
					Type:   "bomb",
					Player: 1,
					Position: engine.Position{
						X: 5,
						Y: 5,
					},
				},
				{
					ID:       "agent/1",
					Type:     "agent",
					Player:   0,
					Position: pos,
					State: engine.State{
						Name: "moving",
					},
					AttrsInt: map[string]int{
						// speed in percentage
						// means "percentage of cell per frame"
						"Speed":       speed,
						percentageKey: percentage,
						"Direction":   int(direction),
					},
				},
			},
		}
	}
	tests := []struct {
		what               string
		expected           string
		speed              int
		direction          engine.GridDirection
		framesToWait       int
		currentPos         engine.Position
		currentPercentage  int
		expectedPos        engine.Position
		expectedPercentage int
		expectedState      string
	}{
		{
			what:               "10% per frame for 11 frames right into the wall",
			expected:           "stop by the wall",
			speed:              10,
			direction:          engine.Rightward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 3, Y: 5},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 4, Y: 5},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "10% per frame for 11 frames left into the wall",
			expected:           "stop by the wall",
			speed:              10,
			direction:          engine.Leftward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 7, Y: 5},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 6, Y: 5},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "10% per frame for 11 frames down into the wall",
			expected:           "stop by the wall",
			speed:              10,
			direction:          engine.Downward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 5, Y: 3},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 5, Y: 4},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
		{
			what:               "10% per frame for 11 frames up into the wall",
			expected:           "stop by the wall",
			speed:              10,
			direction:          engine.Upward,
			framesToWait:       11,
			currentPos:         engine.Position{X: 5, Y: 7},
			currentPercentage:  0,
			expectedPos:        engine.Position{X: 5, Y: 6},
			expectedPercentage: 0,
			expectedState:      "stop",
		},
	}

	var err error
	for _, test := range tests {
		currentState := makeState(
			test.currentPos,
			test.speed,
			test.currentPercentage,
			test.direction,
		)

		for i := 0; i < test.framesToWait; i++ {
			currentState, err = game.MovementRule(20, 20).Resolve(
				currentState,
				[]engine.Command{},
			)
			if err != nil {
				t.Errorf("\n     what: %s\nexpecting: %s\nunexpected error on step %d: %s",
					test.what, test.expected, i, err)
			}
		}

		agent := engine.
			From(currentState.Objects).
			Where(engine.ObjectIsType("agent")).
			All()[0]

		finalPos, finalPercentage, finalState :=
			agent.Position,
			agent.AttrInt(getPercentageKey(test.direction)),
			agent.State.Name

		if want, have := test.expectedPos, finalPos; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
		if want, have := test.expectedPercentage, finalPercentage; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
		if want, have := test.expectedState, finalState; want != have {
			t.Errorf("\n     what: %s\nexpecting: %s\n     want: %v\n     have: %v",
				test.what, test.expected, want, have)
		}
	}
}
