package game

import (
	"fmt"
	"math"
	"sync"

	"github.com/ahmetb/go-linq"

	"gitlab.com/hkcota/bot-challenge/pkg/engine"
)

// BombPlacementRule handles bomb placement commands
func BombPlacementRule() engine.RuleFunc {
	return func(
		currentState engine.Frame,
		commands []engine.Command,
	) (nextState engine.Frame, err error) {
		nextState = currentState

		l := len(commands)
		bombCommands := make([]engine.Command, 0, l)
		bombCriterias := make([]engine.ObjectCriteria, 0, l)

		// get all the bomb placement commands
		linq.
			From(commands).
			WhereT(func(command engine.Command) bool {
				if command.Verb == "place" && command.Target == "bomb" {
					return true
				}
				return false
			}).
			ToSlice(&bombCommands)

		// make sure there is no 2 commands issued to the same player
		// TODO: move this logic to client command validation.
		for i, l := 0, len(bombCommands); i < l; i++ {
			for j := 0; j < l; j++ {
				if i == j {
					continue
				}
				if bombCommands[i].Agent == bombCommands[j].Agent {
					err = engine.ErrorCommandConflict(
						bombCommands[i], bombCommands[j],
						"cannot issue 2 commands to the same agent",
					)
					return
				}
			}
		}

		// assumed only 1 criteria for each agent
		linq.
			From(bombCommands).
			SelectT(func(command engine.Command) engine.ObjectCriteria {
				return func(object engine.Object) bool {
					if object.ID == command.Agent && object.Type == "agent" {
						return true
					}
					return false
				}
			}).
			ToSlice(&bombCriterias)

		// find all agents with the given criteria
		forks := engine.
			From(nextState.Objects).
			Fork(bombCriterias...)

		// examine all commands and create bombs.
		// TODO: change this logic to declarative style.
		newBombs := make([]engine.Object, len(forks))
		for i := range bombCommands {
			agents := forks[i].All()
			if len(agents) < 1 {
				err = fmt.Errorf("Agent(%v) not found", bombCommands[i].Agent)
				return
			}
			if agents[0].Player != bombCommands[i].Player {
				err = fmt.Errorf("Player(%v) does not control Agent(%v)",
					bombCommands[i].Player, bombCommands[i].Agent)
				return
			}
			newBombs[i] = engine.Object{
				ID:       engine.RandObjectID(),
				Type:     "bomb",
				Player:   0,
				Position: agents[0].Position,
				State: engine.State{
					Name:      "will-explode",
					CountDown: 10,
				},
			}
		}

		// append the bombs into nextState.Objects
		nextState.Objects = append(nextState.Objects, newBombs...)
		return
	}
}

// resolveExplosion resolve a read channel of bomb,
// with reference to grid and resolved objects, to
// a slice of resolved objects (agent, wall, bomb)
// and a new read channel of unresolved exploding bomb.
//
// Note that the resolved map contains both bombs
// and side-effects (agent, wall).
func resolveExplosion(
	grid engine.PositionGrid,
	resolved map[engine.ObjectID]engine.Object,
	toResolve <-chan engine.Object,
) (
	nextResolved map[engine.ObjectID]engine.Object,
	nextToResolve <-chan engine.Object,
) {
	wg := sync.WaitGroup{}
	toExplode := make(chan engine.Object)

	sideEffectsWG := sync.WaitGroup{}
	sideEffects := make(chan engine.Object)

	resolveBombExplosion := func(object engine.Object) engine.Object {

		// set state to "exploding" and reset countdown
		object.State.Name = "exploding"
		object.State.CountDown = 10

		// determine explosion range and chained items
		power, _ := object.AttrsInt["Power"]
		for d := range engine.GridDirections() {

			// get all cells on the direction
			cells := grid.FromDirection(
				object.Position.X,
				object.Position.Y,
				d,
				power,
			)

			// kill agents on the bomb
			agents := grid.From(
				object.Position.X,
				object.Position.Y,
			).Where(engine.ObjectIsType("agent")).All()
			for _, agent := range agents {
				sideEffectsWG.Add(1)
				go func(agent engine.Object) {
					agent.State.Name = "dying"
					agent.State.CountDown = 1
					sideEffects <- agent
					sideEffectsWG.Done()
				}(agent)
			}

			// Note: not handling:
			//  a) wall on bomb; or
			//  b) bomb on bomb

			i := 0 // will be used to determine explosion reach
			for ; i < power; i++ {
				forks := cells[i].Fork(
					engine.ObjectIsType("wall"),
					engine.ObjectIsType("agent"),
					func(object engine.Object) bool {
						// select all unexploded bombs
						// that has not been resolved as "exploding".
						if _, ok := resolved[object.ID]; ok {
							return false
						}
						if object.Type != "bomb" {
							return false
						}
						if object.State.Name != "will-explode" {
							return false
						}
						return true
					},
				)

				agents := forks[1].All()
				for _, agent := range agents {
					sideEffectsWG.Add(1)
					go func(agent engine.Object) {
						agent.State.Name = "dying"
						agent.State.CountDown = 1
						sideEffects <- agent
						sideEffectsWG.Done()
					}(agent)
				}

				bombs := forks[2].All()
				for _, bomb := range bombs {
					if _, ok := resolved[bomb.ID]; ok {
						continue // skip resolved bombs to prevent cyclo
					}
					wg.Add(1)
					go func(bomb engine.Object) {
						toExplode <- bomb
						wg.Done()
					}(bomb)
				}
				if len(bombs) > 0 {
					// break the loop, because the explosion can
					// go no further than this bomb cell.
					break
				}
				// TODO: if a bomb on this direction has
				//       already exploded, might need to rethink
				//       the explosion resolution...

				walls := forks[0].All()
				if len(walls) > 0 {
					sideEffectsWG.Add(1)
					go func(wall engine.Object) {
						wall.State.Name = "destroying"
						wall.State.CountDown = 10
						sideEffects <- wall
						sideEffectsWG.Done()
					}(walls[0])
					// break the loop, because the explosion can
					// go no further than this walled cell.
					break
				}
			}

			// remember the extend of explosion
			directionPower := i + 1
			if directionPower > power {
				directionPower = power
			}
			object.AttrsInt["Explosion:"+d.String()] = directionPower
		}
		return object
	}

	// resolve all items involved exactly once
	exploding := engine.FromChannel(toResolve).Map(resolveBombExplosion).All()
	for i := range exploding {
		resolved[exploding[i].ID] = exploding[i]
	}

	// loop through side effects
	go func() {
		sideEffectsWG.Wait()
		close(sideEffects)
	}()
	for sideEffect := range sideEffects {
		resolved[sideEffect.ID] = sideEffect
	}

	// toExplode will be exported
	// this go routine will resolve to close
	// it after it is used.
	go func() {
		wg.Wait()
		close(toExplode)
	}()
	return resolved, toExplode
}

// ExplosionRule handles bomb explosions
func ExplosionRule() engine.RuleFunc {
	return func(
		currentState engine.Frame,
		commands []engine.Command,
	) (nextState engine.Frame, err error) {
		nextState = currentState

		// fork objects into 2 streams
		// 1. bomb(s) that is/are due to explode.
		// 2. bomb(s) and agent(s) that can be affected by bomb.
		toExplode := engine.
			From(nextState.Objects).
			Where(
				func(object engine.Object) bool {
					if object.Type != "bomb" {
						return false
					}
					if object.State.Name != "will-explode" {
						return false
					}
					if object.State.CountDown > 1 {
						return false
					}
					return true
				},
			).
			All()
		if len(toExplode) == 0 {
			// early return if no bombs to resolve
			return
		}

		// formulate a grid to query from
		// TODO: frame setup need to have grid dimension.
		// TODO: use grid dimension from frame setup.
		grid := engine.New2DGrid(20, 20, nextState.Objects)

		// generate a stream of bomb objects
		// for first round resolve
		toResolve := engine.
			From(toExplode).
			ToChannel()

		// resolve explosions until there is nothing new explosion
		// resolved in the loop.
		//
		// Note that the prove of no new explosion is to have
		// no change in the resolved map. So this loop will run
		// for 1 more time even if there is nothing new in the
		// toResolve channel.
		resolved := make(map[engine.ObjectID]engine.Object)
		for i, j := -1, len(resolved); i != j; i, j = j, len(resolved) {
			resolved, toResolve = resolveExplosion(grid, resolved, toResolve)
		}

		// assign all resolved objects accordingly
		nextState.Objects = engine.
			From(nextState.Objects).
			Map(func(object engine.Object) engine.Object {
				if newObject, ok := resolved[object.ID]; ok {
					return newObject
				}
				return object
			}).
			All()

		return
	}
}

// MovementRule generates rule that continue
// movements.
func MovementRule(gridWidth, gridHeight int) engine.RuleFunc {
	return func(
		currentState engine.Frame,
		commands []engine.Command,
	) (nextState engine.Frame, err error) {
		nextState = currentState

		// TODO: gird size from initial state
		gridWidth, gridHeight := 20, 20

		forks := engine.
			From(nextState.Objects).
			Fork(
				func(object engine.Object) bool {
					if object.Type == "agent" && object.State.Name == "moving" {
						return true
					}
					return false
				},
				engine.ObjectAny,
			)

		// formulate a grid with only obstacle mapped
		// TODO: frame setup need to have grid dimension.
		// TODO: use grid dimension from frame setup.
		grid := engine.New2DGrid(
			20,
			20,
			engine.
				From(nextState.Objects).
				Where(engine.Or(
					engine.ObjectIsType("wall"),
					engine.ObjectIsType("bomb"),
				)).
				All(),
		)

		forks[0] = forks[0].
			Map(func(object engine.Object) engine.Object {
				// check commands
				// Note: expect 1 command for each agent
				//       in any given frame.
				for i := range commands {
					if commands[i].Player != object.Player {
						continue // if the player is not object owner, pass
					}
					if commands[i].Agent == object.ID && commands[i].Verb == "move" {
						speed, direction := object.AttrInt("Speed"), engine.Leftward
						percentageKey, otherKey := "Percentage:x", "Percentage:y"
						switch commands[i].Target {
						case "left":
							// do nothing
						case "right":
							direction = engine.Rightward
						case "up":
							direction = engine.Upward
							percentageKey, otherKey = otherKey, percentageKey
						case "down":
							direction = engine.Downward
							percentageKey, otherKey = otherKey, percentageKey
						default:
							// do nothing
							// TODO: some error detection?
							break
						}

						if otherPercentage := object.AttrInt(otherKey); otherPercentage >= speed || otherPercentage <= -speed {
							// if exceed position 0 more than speed (movement for 1 frame),
							// blocked and stop instead
							object.State.Name = "stop"
							object.State.CountDown = 0
							object.AttrsInt["Direction"] = int(direction)
							break
						}

						// round down the otherKey value
						object.State.Name = "moving"
						object.State.CountDown = 0
						object.AttrsInt[otherKey] = 0
						object.AttrsInt["Direction"] = int(direction)
						break
					}
				}
				return object
			}).
			Map(func(object engine.Object) engine.Object {
				// only resolve movement if the object is moving
				if object.State.Name != "moving" {
					return object
				}

				// after movement command, the state should auto resolve
				direction := engine.GridDirection(object.AttrInt("Direction"))
				speed, percentageKey, factor := object.AttrInt("Speed"), "Percentage:x", 1

				switch direction {
				case engine.Upward:
					percentageKey = "Percentage:y"
					factor = -1
				case engine.Downward:
					percentageKey = "Percentage:y"
					// factor = 1
				case engine.Rightward:
					// percentageKey = "Percentage:x"
					// factor = 1
				case engine.Leftward:
					// percentageKey = "Percentage:x"
					factor = -1
				}

				// remove directions of percentage with *factor
				// then evaluate absolute steps and percentage.
				nextPercentage, steps := object.AttrInt(percentageKey)*factor+speed, 0
				for ; nextPercentage > 100; steps, nextPercentage = steps+1, nextPercentage-100 {
				}
				if nextPercentage >= 50 {
					steps++
					nextPercentage = nextPercentage - 100
				}

				lookAhead := int(math.Ceil(float64(steps) + float64(nextPercentage)*0.01))
				movementPath := grid.FromDirection(
					object.Position.X,
					object.Position.Y,
					direction,
					lookAhead,
				)

				// if there is some obstacle on the way,
				// trigger state changed to "stop".
				for i := range movementPath {
					if movementPath[i] != nil {
						// something is ahead, trigger stop
						if i < steps {
							steps = i - 1
						}
						object.State.Name = "stop"
						nextPercentage = 0
						break
					}
				}

				// assign percentage and position with direction *factor
				if percentageKey == "Percentage:x" {
					object.Position.X += steps * factor
				} else {
					object.Position.Y += steps * factor
				}
				object.AttrsInt[percentageKey] = nextPercentage * factor

				// check final positions against boundaries
				// handle out of bound positions
				if object.Position.X < 1 {
					object.State.Name = "stop"
					object.State.CountDown = 0
					object.Position.X = 1
				} else if object.Position.X > gridWidth {
					object.State.Name = "stop"
					object.State.CountDown = 0
					object.Position.X = gridWidth
				}
				if object.Position.Y < 1 {
					object.State.Name = "stop"
					object.State.CountDown = 0
					object.Position.Y = 1
				} else if object.Position.Y > gridHeight {
					object.State.Name = "stop"
					object.State.CountDown = 0
					object.Position.Y = gridHeight
				}

				if object.Position.X == 1 && object.AttrsInt["Percentage:x"] < 0 {
					object.State.Name = "stop"
					object.State.CountDown = 0
					object.AttrsInt["Percentage:x"] = 0
				} else if object.Position.X == gridWidth && object.AttrsInt["Percentage:x"] > 0 {
					object.State.Name = "stop"
					object.State.CountDown = 0
					object.AttrsInt["Percentage:x"] = 0
				}
				if object.Position.Y == 1 && object.AttrsInt["Percentage:y"] < 0 {
					object.State.Name = "stop"
					object.State.CountDown = 0
					object.AttrsInt["Percentage:y"] = 0
				} else if object.Position.Y == gridHeight && object.AttrsInt["Percentage:y"] > 0 {
					object.State.Name = "stop"
					object.State.CountDown = 0
					object.AttrsInt["Percentage:y"] = 0
				}

				return object
			})

		nextState.Objects = engine.MergeStreams(forks...).All()
		return
	}
}
