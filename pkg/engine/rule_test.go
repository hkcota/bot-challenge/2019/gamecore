package engine_test

import (
	"fmt"
	"testing"

	"gitlab.com/hkcota/bot-challenge/pkg/engine"
)

func TestResolve(t *testing.T) {

	// setup
	currentState := engine.Frame{
		ID:     1,
		NextID: 2,
	}
	commands := []engine.Command{
		{
			Verb:   "hello",
			Target: "world",
		},
	}
	rules := []engine.Rule{
		engine.RuleFunc(func(
			currentState engine.Frame,
			commands []engine.Command,
		) (nextState engine.Frame, err error) {
			for _, command := range commands {
				nextState.NextID, err = 0, fmt.Errorf("%s %s", command.Verb, command.Target)
			}
			return
		}),
	}

	// statement to test
	nextState, err := engine.Resolve(
		currentState,
		commands,
		rules,
	)

	// inspect result
	if err == nil {
		t.Errorf("expected error, got nil")
	} else if want, have := "step(0): hello world", err.Error(); want != have {
		t.Errorf("expected: %#v, got: %#v", want, have)
	}
	if want, have := engine.FrameID(0), nextState.NextID; want != have {
		t.Errorf("expected: %d, got: %d", want, have)
	}
}

func TestCommand_String(t *testing.T) {
	c1 := engine.Command{
		Player: 12,
		Agent:  "agent/123",
		Verb:   "fly",
	}
	c2 := engine.Command{
		Player: 24,
		Agent:  "agent/456",
		Verb:   "say",
		Target: "hello",
	}

	if want, have := "Player(12) command Agent(agent/123) to fly", c1.String(); want != have {
		t.Errorf("\nexpected: %s\n     got: %s", want, have)
	}
	if want, have := "Player(24) command Agent(agent/456) to say hello", c2.String(); want != have {
		t.Errorf("\nexpected: %s\n     got: %s", want, have)
	}
}
