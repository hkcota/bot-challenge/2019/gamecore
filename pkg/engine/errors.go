package engine

import "fmt"

// CommandConflict illustrates
type CommandConflict struct {
	cmd1 Command
	cmd2 Command
	msg  string
}

// ErrorCommandConflict produces a command conflict error
func ErrorCommandConflict(cmd1, cmd2 Command, msg string) *CommandConflict {
	return &CommandConflict{
		cmd1, cmd2, msg,
	}
}

// Error implements error interface
func (c CommandConflict) Error() string {
	return fmt.Sprintf(
		"Conflict between %#v and %#v: %s",
		c.cmd1, c.cmd2, c.msg,
	)
}

// Command1 return the first command in conflict
func (c CommandConflict) Command1() Command {
	return c.cmd1
}

// Command2 return the second command in conflict
func (c CommandConflict) Command2() Command {
	return c.cmd1
}

// Msg return the message about the conflict
func (c CommandConflict) Msg() string {
	return c.msg
}
