package engine

import (
	"context"
	"reflect"
	"sync"
)

// Objects is collection of objects
type Objects []Object

// ObjectStream contains way to select and manipulate
// Objects
type ObjectStream struct {
	src   objectSource
	pipes []objectPipe
}

// objectSource stores an Object stream generator, and
// a cancel channel that could break the object stream.
type objectSource func() (out <-chan *objectState, cancelFunc context.CancelFunc)

// objectPipe stores procedure that would either filter
// or transform the given object stream.
type objectPipe func(in <-chan *objectState) (out <-chan *objectState)

type objectState struct {
	key   int
	value *Object
}

// clone makes a copy of the current stream.
func (stream *ObjectStream) clone() *ObjectStream {
	l := len(stream.pipes)
	c := ObjectStream{
		src:   stream.src,
		pipes: make([]objectPipe, l, l+1),
	}
	copy(c.pipes, stream.pipes)
	return &c
}

// appendPipe appends a new objectPipe at the end
// of the whole pipeline.
func (stream *ObjectStream) appendPipe(p objectPipe) {
	stream.pipes = append(stream.pipes, p)
}

// eval generates output from the given source
// and pipes.
func (stream *ObjectStream) eval() (<-chan *objectState, context.CancelFunc) {
	if stream == nil {
		// handle nil stream
		out, cancelFunc := make(chan *objectState), func() {}
		close(out)
		return out, cancelFunc
	}

	out, cancelFunc := stream.src()
	for _, pipe := range stream.pipes {
		out = pipe(out)
	}
	return out, cancelFunc
}

// From defines ObjectStream
func From(objects Objects) *ObjectStream {
	return &ObjectStream{
		src: func() (<-chan *objectState, context.CancelFunc) {
			out, toCancel := make(chan *objectState), make(chan chan<- int)
			cancel := func() {
				close(toCancel)
			}
			go func() {
			genLoop:
				for key := range objects {
					select {
					case out <- &objectState{key: key, value: &objects[key]}:
						// normal operation
					case <-toCancel:
						break genLoop
					}
				}
				close(out)
			}()
			return out, cancel
		},
		pipes: make([]objectPipe, 0, 8),
	}
}

// FromChannel creates ObjecStream from a given object read channel.
func FromChannel(ch <-chan Object) *ObjectStream {
	return &ObjectStream{
		src: func() (<-chan *objectState, context.CancelFunc) {
			out := make(chan *objectState)
			go func() {
				i := 0
				for in := range ch {
					object := in // clone the object before passing on, prevent data race
					out <- &objectState{key: i, value: &object}
					i++
				}
				close(out)
			}()
			return out, func() {}
		},
		pipes: make([]objectPipe, 0, 8),
	}
}

// Where defines criteria for the selection of object
func (stream *ObjectStream) Where(criteria ObjectCriteria) *ObjectStream {
	if stream == nil {
		return nil
	}
	q := stream.clone()
	q.appendPipe(func(in <-chan *objectState) <-chan *objectState {
		out := make(chan *objectState)
		go func() {
			for state := range in {
				if criteria(*state.value) {
					out <- state
				}
			}
			close(out)
		}()
		return out
	})
	return q
}

// Map applys an operation on all the objects that matches the
// current criteria, then pass them on.
func (stream *ObjectStream) Map(do func(Object) Object) (q *ObjectStream) {
	q = stream.clone()
	q.appendPipe(func(in <-chan *objectState) <-chan *objectState {
		out := make(chan *objectState)
		go func() {
			for state := range in {
				*state.value = do(*state.value)
				out <- state
			}
			close(out)
		}()
		return out
	})
	return
}

// ToChannel break out the objects stream to channel.
func (stream *ObjectStream) ToChannel() <-chan Object {
	out := make(chan Object)
	in, _ := stream.eval()
	go func() {
		for state := range in {
			out <- *state.value
		}
		close(out)
	}()
	return out
}

// All will return results that matches the current criteria,
// in random order, that matches the given criteria.
func (stream *ObjectStream) All() (objects Objects) {
	out, _ := stream.eval()
	objects = make(Objects, 0, 20)
	for state := range out {
		objects = append(objects, *state.value)
	}
	return
}

// Count the number of objects containing.
func (stream *ObjectStream) Count() int {
	out, _ := stream.eval()
	i := 0
	for range out {
		i++
	}
	return i
}

func nonblockingPipe(in <-chan *objectState) <-chan *objectState {
	out := make(chan *objectState)

	// store all upstream values in goroutine
	// and pending for write to out.
	//
	// upstream will never be blocked.
	// the only matter is the number of goroutines
	// in the memory.
	go func() {
		wg := sync.WaitGroup{}
		for {
			if state, ok := <-in; ok {
				wg.Add(1)
				go func(state *objectState) {
					out <- state
					wg.Done()
				}(state)
				continue
			}
			break
		}

		// wait until all states are send out,
		// then close the output chanel
		wg.Wait()
		close(out)
	}()

	return out
}

// Fork the current ObjectStream into separated streams
// defined by the given criteria.
//
// Objects from upstream will be tested against criterias
// one by one. If the object matches the first criteria,
// it is passed only to the 1st stream. If not, the object
// will be tested against the 2nd criteria. So forth and
// so on.Fork
//
// Objects from upstream that matches no criteria will
// be discarded.
func (stream *ObjectStream) Fork(criterias ...ObjectCriteria) (forks []*ObjectStream) {
	l := len(criterias)
	forks = make([]*ObjectStream, l)
	forkIns := make([]chan *objectState, l)

	// build output object streams
	for i := 0; i < l; i++ {
		forkIns[i] = make(chan *objectState)
		downstream := nonblockingPipe(forkIns[i])
		forks[i] = &ObjectStream{
			src: func() (<-chan *objectState, context.CancelFunc) {
				// TODO: use the upstream cancel function instead of this dummy.
				return downstream, func() {}
			},
			pipes: make([]objectPipe, 0, 8),
		}
	}

	// feeds to forkIns, which are non-blocking
	// because of the nonblockingPipe passing to downstream
	go func() {
		// TODO: should block this from starting until any
		// of the forks are evaluated.
		in, _ := stream.eval()
		for state := range in {
			for i, criteria := range criterias {
				if criteria(*state.value) {
					// if the object match the criteria
					// send to the releveant fork
					// (expected to be always nonblocking).
					forkIns[i] <- state
					break
				}
			}
		}

		// when all the state from upstream are sent,
		// close all the forkIns channel. expect
		// downstream pipes to handle this gracefully.
		for i := range forkIns {
			close(forkIns[i])
		}
	}()

	return
}

// MergeStreams n *ObjectStream stream into one.
func MergeStreams(streams ...*ObjectStream) *ObjectStream {
	src := func() (<-chan *objectState, context.CancelFunc) {
		l, out := len(streams), make(chan *objectState)
		cases, upstreamCancels := make([]reflect.SelectCase, l), make([]context.CancelFunc, l)

		// reflect cases to join the channel
		for i := 0; i < l; i++ {
			var upstreamPipe <-chan *objectState
			upstreamPipe, upstreamCancels[i] = streams[i].eval()
			cases[i] = reflect.SelectCase{
				Dir:  reflect.SelectRecv,
				Chan: reflect.ValueOf(upstreamPipe),
			}
		}

		// run all the cases until all pipes are closed
		go func() {
			for {
				i, v, ok := reflect.Select(cases)
				if !ok {
					cases = append(cases[:i], cases[i+1:]...)
					if len(cases) == 0 {
						break
					}
					continue
				}
				out <- v.Interface().(*objectState)
			}
			close(out)
		}()

		// trigger on inner cancelFuncs at outer
		cancelFunc := func() {
			for _, upstreamCancel := range upstreamCancels {
				upstreamCancel()
			}
		}
		return out, cancelFunc
	}

	return &ObjectStream{
		src:   src,
		pipes: make([]objectPipe, 0, 8),
	}
}

// ObjectCriteria is the function to filter
// objects with.
type ObjectCriteria func(object Object) bool

// And join multiple criterias into one by "and".
func And(criterias ...ObjectCriteria) ObjectCriteria {
	return func(object Object) bool {
		for i := range criterias {
			if !criterias[i](object) {
				return false
			}
		}
		return true
	}
}

// Or join multiple criterias into one by "and".
func Or(criterias ...ObjectCriteria) ObjectCriteria {
	return func(object Object) bool {
		for i := range criterias {
			if criterias[i](object) {
				return true
			}
		}
		return false
	}
}

// ObjectIsType check if an object given is of type t.
func ObjectIsType(t string) ObjectCriteria {
	return func(object Object) bool {
		if object.Type == t {
			return true
		}
		return false
	}
}

// ObjectHasID check if an object given has the
// specific ID.
func ObjectHasID(id ObjectID) ObjectCriteria {
	return func(object Object) bool {
		if object.ID == id {
			return true
		}
		return false
	}
}

// ObjectHasEitherID check if an object has one
// of the IDs given in the slice.
func ObjectHasEitherID(ids ...ObjectID) ObjectCriteria {
	return func(object Object) bool {
		for j, l := 0, len(ids); j < l; j, l = j+1, len(ids) {
			if object.ID == ids[j] {
				return true
			}
		}
		return false
	}
}

// ObjectAny passes anyway. Selects everything when used
// with ObjectStream.Fork or ObjectStream.Where.
func ObjectAny(Object) bool {
	return true
}
