package engine

import "github.com/gofrs/uuid"

// PlayerID is the ID type of any player
type PlayerID int

// FrameID is the ID type of any frame.
type FrameID int

// Frame is a snapshot of the game status
// at a time.
//
// All Actors are entitled to at most 1 action
// per Frame.
type Frame struct {
	ID      FrameID
	NextID  FrameID
	Objects Objects
}

// State represents abstration of state of any
// game object.
type State struct {
	Name      string
	CountDown int
}

// Position represents an object's positiion on
// the game board.
type Position struct {
	X int
	Y int
}

// ObjectID is the ID type of object.
type ObjectID string

// RandObjectID allocates new random object ID.
func RandObjectID() ObjectID {
	return ObjectID(uuid.Must(uuid.NewV4()).String())
}

// Object represents abstracted game objects in
// in a game environment (e.g. character, bomb, wall).
type Object struct {
	ID       ObjectID
	Player   PlayerID
	Type     string
	Position Position
	State    State
	AttrsInt map[string]int
}

// AttrInt is a simplified interface to access AttrsInt values.
// If an attribute is unset, it will always return 0.
func (object Object) AttrInt(key string) (val int) {
	val, _ = object.AttrsInt[key]
	return
}
