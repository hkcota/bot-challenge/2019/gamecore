package engine_test

import (
	"testing"

	"gitlab.com/hkcota/bot-challenge/pkg/engine"
)

func TestPositionGrid_From(t *testing.T) {

	grid := engine.New2DGrid(5, 5, engine.Objects{
		engine.Object{
			ID: "object/1",
			Position: engine.Position{
				X: 3,
				Y: 3,
			},
		},
		engine.Object{
			ID: "object/2",
			Position: engine.Position{
				X: 3,
				Y: 2,
			},
		},
		engine.Object{
			ID: "object/3",
			Position: engine.Position{
				X: 3,
				Y: 1,
			},
		},
		engine.Object{
			ID: "object/4",
			Position: engine.Position{
				X: 2,
				Y: 3,
			},
		},
		engine.Object{
			ID: "object/5",
			Position: engine.Position{
				X: 1,
				Y: 3,
			},
		},
		engine.Object{
			ID: "object/6",
			Position: engine.Position{
				X: 3,
				Y: 3,
			},
		},
	})

	var stacked engine.Objects

	stacked = grid.From(3, 3).All()
	if want, have := 2, len(stacked); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
		t.FailNow()
	}
	if want, have := engine.ObjectID("object/1"), stacked[0].ID; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if want, have := engine.ObjectID("object/6"), stacked[1].ID; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	stacked = grid.From(3, 1).All()
	if want, have := 1, len(stacked); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
		t.FailNow()
	}
	if want, have := engine.ObjectID("object/3"), stacked[0].ID; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func TestPositionGrid_From_empty(t *testing.T) {
	grid := engine.New2DGrid(10, 10, engine.Objects{
		engine.Object{
			Position: engine.Position{
				X: 5,
				Y: 5,
			},
		},
	})

	var v *engine.ObjectStream
	if want, have := v, grid.From(-1, 1); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if want, have := v, grid.From(1, -1); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if want, have := v, grid.From(10, 1); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if want, have := v, grid.From(1, 10); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if want, have := v, grid.From(10, 5); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if want, have := v, grid.From(5, 10); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func TestPositionGrid_FromDirection(t *testing.T) {

	grid := engine.New2DGrid(5, 5, engine.Objects{
		engine.Object{
			ID: "object/1",
			Position: engine.Position{
				X: 3,
				Y: 3,
			},
		},
		engine.Object{
			ID: "object/2",
			Position: engine.Position{
				X: 3,
				Y: 2,
			},
		},
		engine.Object{
			ID: "object/3",
			Position: engine.Position{
				X: 3,
				Y: 1,
			},
		},
		engine.Object{
			ID: "object/4",
			Position: engine.Position{
				X: 2,
				Y: 3,
			},
		},
		engine.Object{
			ID: "object/5",
			Position: engine.Position{
				X: 1,
				Y: 3,
			},
		},
		engine.Object{
			ID: "object/6",
			Position: engine.Position{
				X: 3,
				Y: 3,
			},
		},
	})

	var stack []*engine.ObjectStream

	stack = grid.FromDirection(3, 3, engine.Upward, 2)
	if want, have := 2, len(stack); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
		t.FailNow()
	}
	if stack[0] == nil {
		t.Errorf("expected proper query, got nil")
		t.FailNow()
	}
	if want, have := 1, stack[0].Count(); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
	}
	if want, have := engine.ObjectID("object/2"), stack[0].All()[0].ID; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if stack[1] == nil {
		t.Errorf("expected proper query, got nil")
		t.FailNow()
	}
	if want, have := 1, stack[1].Count(); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
	}
	if want, have := engine.ObjectID("object/3"), stack[1].All()[0].ID; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	stack = grid.FromDirection(3, 1, engine.Downward, 2)
	if want, have := 2, len(stack); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
		t.FailNow()
	}
	if stack[0] == nil {
		t.Errorf("expected proper query, got nil")
		t.FailNow()
	}
	if want, have := 1, stack[0].Count(); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
	}
	if want, have := engine.ObjectID("object/2"), stack[0].All()[0].ID; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if stack[1] == nil {
		t.Errorf("expected proper query, got nil")
		t.FailNow()
	}
	if want, have := 2, stack[1].Count(); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
	}
	if want, have := engine.ObjectID("object/1"), stack[1].All()[0].ID; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	stack = grid.FromDirection(3, 3, engine.Leftward, 2)
	if want, have := 2, len(stack); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
		t.FailNow()
	}
	if stack[0] == nil {
		t.Errorf("expected proper query, got nil")
		t.FailNow()
	}
	if want, have := 1, stack[0].Count(); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
	}
	if want, have := engine.ObjectID("object/4"), stack[0].All()[0].ID; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if stack[1] == nil {
		t.Errorf("expected proper query, got nil")
		t.FailNow()
	}
	if want, have := 1, stack[1].Count(); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
	}
	if want, have := engine.ObjectID("object/5"), stack[1].All()[0].ID; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}

	stack = grid.FromDirection(1, 3, engine.Rightward, 2)
	if want, have := 2, len(stack); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
		t.FailNow()
	}
	if stack[0] == nil {
		t.Errorf("expected proper query, got nil")
		t.FailNow()
	}
	if want, have := 1, stack[0].Count(); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
	}
	if want, have := engine.ObjectID("object/4"), stack[0].All()[0].ID; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	if stack[1] == nil {
		t.Errorf("expected proper query, got nil")
		t.FailNow()
	}
	if want, have := 2, stack[1].Count(); want != have {
		t.Errorf("expected %d objects, got %d", want, have)
	}
	if want, have := engine.ObjectID("object/1"), stack[1].All()[0].ID; want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}
