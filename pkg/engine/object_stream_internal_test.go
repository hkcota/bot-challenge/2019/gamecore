package engine

import (
	"testing"
	"time"
)

func Test_nonblockingPipe(t *testing.T) {
	in := make(chan *objectState)
	done := make(chan int)
	out := nonblockingPipe(in)
	testSize := 5

	go func(testSize int) {
		defer close(done)
		for i := 0; i < testSize; i++ {
			in <- nil
		}
		close(in)
	}(testSize)

	select {
	case <-time.After(5 * time.Millisecond):
		t.Errorf("Time out. Probably blocked")
	case <-done:
		t.Logf("done")
	}

	var i int
	for range out {
		i++
	}
	if want, have := testSize, i; want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
}
