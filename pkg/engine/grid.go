package engine

import (
	"fmt"
)

// GridDirection represents the direction on a 2D grid
type GridDirection int

// String implements Stringer
func (d GridDirection) String() string {
	switch d {
	case Upward:
		return "up"
	case Rightward:
		return "right"
	case Downward:
		return "down"
	case Leftward:
		return "left"
	}
	return fmt.Sprintf("unknown GridDireciton(%d)", d)
}

const (
	// NoDirection means it is not toward any direction
	NoDirection GridDirection = iota

	// Upward direction
	Upward

	// Rightward direction
	Rightward

	// Downward direction
	Downward

	// Leftward direction
	Leftward
)

// GridDirections return channel that range
// throught all directions (to reduce mistakes in
// using the consts).
func GridDirections() <-chan GridDirection {
	out := make(chan GridDirection)
	go func() {
		for i := Upward; i <= Leftward; i++ {
			out <- i
		}
		close(out)
	}()
	return out
}

// PositionGrid is a lookup interface for
// looking up objects within a 2D grid
// coordinate.
//
// Coordiations are [x][y]Objects
type PositionGrid [][]Objects

// New2DGrid creates a 2D position grid of width and height, filling
// with objects given to it.
func New2DGrid(width, height int, objects Objects) (grid PositionGrid) {
	grid = make([][]Objects, width)
	for x := 0; x < width; x++ {
		grid[x] = make([]Objects, height)
	}

	for i := range objects {
		if grid[objects[i].Position.X][objects[i].Position.Y] == nil {
			grid[objects[i].Position.X][objects[i].Position.Y] = make(Objects, 0, 10)
		}
		grid[objects[i].Position.X][objects[i].Position.Y] =
			append(grid[objects[i].Position.X][objects[i].Position.Y], objects[i])
	}
	return
}

// From gets an *ObjectStream from objects at the positon X, Y.
// The *ObjectStream will be nil if there is no object at the position.
func (grid PositionGrid) From(x, y int) *ObjectStream {
	if x < 0 || y < 0 {
		return nil
	}
	if x >= len(grid) {
		return nil
	}
	if grid[x] == nil {
		return nil
	}
	if y >= len(grid[x]) {
		return nil
	}
	if grid[x][y] == nil {
		return nil
	}
	return From(grid[x][y])
}

// FromDirection gets []*ObjectStream from the position x, y
// on the direction d of the length l. If l < 0, returns empty
// slice.
//
// The item 0 of the results is the objects on the 1st cell
// on that direction. Index 1 is for the objects on the 2nd
// cell. So on and so forth.
func (grid PositionGrid) FromDirection(x, y int, d GridDirection, l int) (results []*ObjectStream) {
	if l < 0 {
		return make([]*ObjectStream, 0)
	}

	results = make([]*ObjectStream, l)
	switch d {
	case Upward:
		for dy, i := y-1, 0; dy >= 0 && i < l; dy, i = dy-1, i+1 {
			results[i] = grid.From(x, dy)
		}
	case Downward:
		for dy, fy, i := y+1, y+l, 0; dy <= fy && i < l; dy, i = dy+1, i+1 {
			results[i] = grid.From(x, dy)
		}
	case Leftward:
		for dx, i := x-1, 0; dx >= 0 && i < l; dx, i = dx-1, i+1 {
			results[i] = grid.From(dx, y)
		}
	case Rightward:
		for dx, fx, i := x+1, x+l, 0; dx <= fx && i < l; dx, i = dx+1, i+1 {
			results[i] = grid.From(dx, y)
		}
	}
	return
}
