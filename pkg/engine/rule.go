package engine

import "fmt"

// Rule is an abstraction for defining
// game rule.
type Rule interface {
	Resolve(
		currentState Frame,
		commands []Command,
	) (nextState Frame, err error)
}

// RuleFunc is the type to implement
// Rule by pure function.
type RuleFunc func(
	currentState Frame,
	commands []Command,
) (nextState Frame, err error)

// Resolve implements Rule.
func (fn RuleFunc) Resolve(
	currentState Frame,
	commands []Command,
) (nextState Frame, err error) {
	return fn(currentState, commands)
}

// Command is an abstraction of game command send
// to agents by the player.
type Command struct {

	// Player who issued the command.
	Player PlayerID

	// Agent of the Command to take place.
	Agent ObjectID

	// Verb describe the Command to do.
	Verb string

	// Target describe the target of the Command.
	// Only some of the Commands require this
	// (e.g. movement).
	Target string
}

// String implements Stringer interface.
func (cmd Command) String() (msg string) {
	msg = fmt.Sprintf("Player(%v) command Agent(%v) to %v",
		cmd.Player, cmd.Agent, cmd.Verb)
	if cmd.Target != "" {
		msg += " " + cmd.Target
	}
	return
}

// Resolve the next state of a frame
// with the list of command and the
// given Rule interface.
//
// Return the number of commands that
// ran (i), the resulting state, and
// error, if any.
func Resolve(
	currentState Frame,
	commands []Command,
	rules []Rule,
) (nextState Frame, err error) {
	nextState = currentState
	for i := range rules {
		if nextState, err = rules[i].Resolve(nextState, commands); err != nil {
			err = fmt.Errorf("step(%d): %s", i, err)
			return
		}
	}
	return
}
