package engine_test

import (
	"testing"

	"gitlab.com/hkcota/bot-challenge/pkg/engine"
)

func TestRandObjectID(t *testing.T) {
	id1, id2 := engine.RandObjectID(), engine.RandObjectID()

	// test against 2 runs
	if id1 == id2 {
		t.Errorf("expected calling RandObjectID twice got different ID, but both got %v", id1)
	}

	// test against previously generated id
	if id1 == "e47dffe0-637d-4770-a083-daa9e44fe39d" {
		t.Errorf("expected calling RandObjectID be really random, got %s every first run", id1)
	}
}
