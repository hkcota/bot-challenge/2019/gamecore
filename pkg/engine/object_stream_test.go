package engine_test

import (
	"testing"

	"gitlab.com/hkcota/bot-challenge/pkg/engine"
)

func TestObjectStream_All(t *testing.T) {

	objects := engine.From(engine.Objects{
		engine.Object{
			ID: "generic/1",
			State: engine.State{
				Name:      "hello",
				CountDown: 10,
			},
		},
		engine.Object{
			ID: "generic/2	",
			State: engine.State{
				Name:      "world",
				CountDown: 20,
			},
		},
		engine.Object{
			ID: "generic/3",
			State: engine.State{
				Name:      "hello",
				CountDown: 30,
			},
		},
	}).Where(func(object engine.Object) bool {
		if object.State.Name == "hello" {
			return true
		}
		return false
	}).All()

	if want, have := 2, len(objects); want != have {
		t.Errorf("expected len: %d, got: %d", want, have)
		t.FailNow()
	}
	if want, have := 10, objects[0].State.CountDown; want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
	if want, have := 30, objects[1].State.CountDown; want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
}

func TestObjectStream_Map(t *testing.T) {

	result := engine.From(engine.Objects{
		engine.Object{
			ID: "generic/1",
			State: engine.State{
				Name:      "hello",
				CountDown: 10,
			},
		},
		engine.Object{
			ID: "generic/2",
			State: engine.State{
				Name:      "world",
				CountDown: 10,
			},
		},
		engine.Object{
			ID: "generic/3",
			State: engine.State{
				Name:      "hello",
				CountDown: 10,
			},
		},
	}).Map(func(object engine.Object) engine.Object {
		object.State.CountDown = 100
		return object
	}).All()

	if want, have := 3, len(result); want != have {
		t.Errorf("expected len: %d, got: %d", want, have)
		t.FailNow()
	}
	if want, have := 100, result[0].State.CountDown; want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
	if want, have := 100, result[1].State.CountDown; want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
	if want, have := 100, result[2].State.CountDown; want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
}

func TestObjectStream_nil(t *testing.T) {
	var s *engine.ObjectStream

	r := s.Where(func(engine.Object) bool {
		return true
	}).All()

	if want, have := 0, len(r); want != have {
		t.Errorf("expected len = %d, got %d", want, have)
	}
}

func TestObjectStream_Fork(t *testing.T) {
	forks := engine.From(engine.Objects{
		engine.Object{
			ID: "generic/1",
			State: engine.State{
				Name:      "hello",
				CountDown: 10,
			},
		},
		engine.Object{
			ID: "generic/2",
			State: engine.State{
				Name:      "world",
				CountDown: 20,
			},
		},
		engine.Object{
			ID: "generic/3",
			State: engine.State{
				Name:      "hello",
				CountDown: 30,
			},
		},
	}).Fork(
		func(object engine.Object) bool {
			if object.State.Name == "hello" {
				return true
			}
			return false
		},
		func(object engine.Object) bool {
			if object.State.Name == "nothing" {
				return true
			}
			return false
		},
		func(object engine.Object) bool {
			return true // everything else
		},
	)

	if want, have := 3, len(forks); want != have {
		t.Errorf("expected %d forks, got only %d", want, have)
	}

	var results []engine.Object
	results = forks[0].All()
	if want, have := 2, len(results); want != have {
		t.Errorf("expected %d, got %d", want, have)
	}
}

func TestMergeStreams(t *testing.T) {
	q1 := engine.From(engine.Objects{
		engine.Object{
			ID: "generic/1",
			State: engine.State{
				Name:      "hello",
				CountDown: 10,
			},
		},
		engine.Object{
			ID: "generic/2",
			State: engine.State{
				Name:      "world",
				CountDown: 20,
			},
		},
	})
	q2 := engine.From(engine.Objects{
		engine.Object{
			ID: "agent/1",
			State: engine.State{
				Name:      "hello",
				CountDown: 10,
			},
		},
		engine.Object{
			ID: "agent/2",
			State: engine.State{
				Name:      "world",
				CountDown: 20,
			},
		},
	})
	q3 := engine.From(engine.Objects{
		engine.Object{
			ID: "bomb/1",
			State: engine.State{
				Name:      "hello",
				CountDown: 10,
			},
		},
		engine.Object{
			ID: "bomb/2",
			State: engine.State{
				Name:      "world",
				CountDown: 20,
			},
		},
	})

	q := engine.MergeStreams(q1, q2, q3)
	expectedIDs := []engine.ObjectID{
		"generic/1",
		"generic/2",
		"agent/1",
		"agent/2",
		"bomb/1",
		"bomb/2",
	}

	// generate test results
	results := q.All()

	// examine the test results
	if want, have := len(expectedIDs), len(results); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	for _, expectedID := range expectedIDs {
		found := false
		for _, object := range results {
			if object.ID == expectedID {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("ID %#v not found in result objects", expectedID)
		}
	}
}

func TestObjectStream_Fork_Where_MergeStream(t *testing.T) {
	// This is to test the combo use of Fork, Where and MergeStream
	forks := engine.From(engine.Objects{
		engine.Object{
			ID: "generic/1",
			State: engine.State{
				Name:      "hello",
				CountDown: 10,
			},
		},
		engine.Object{
			ID: "generic/2",
			State: engine.State{
				Name:      "world",
				CountDown: 2000,
			},
		},
		engine.Object{
			ID: "generic/3",
			State: engine.State{
				Name:      "hello",
				CountDown: 30,
			},
		},
		engine.Object{
			ID: "generic/4",
			State: engine.State{
				Name:      "hello",
				CountDown: 100,
			},
		},
		engine.Object{
			ID: "generic/5",
			State: engine.State{
				Name:      "hello",
				CountDown: 1000,
			},
		},
	}).Fork(
		func(object engine.Object) bool {
			if object.State.Name == "hello" {
				return true
			}
			return false
		},
		func(object engine.Object) bool {
			if object.State.Name == "nothing" {
				return true
			}
			return false
		},
		func(object engine.Object) bool {
			return true // everything else
		},
	)

	// apply Where in the first stream only
	forks[0] = forks[0].Where(func(object engine.Object) bool {
		if object.State.CountDown <= 100 {
			return true
		}
		return false
	})

	stream := engine.MergeStreams(forks...)
	results := stream.All()

	expectedIDs := []engine.ObjectID{
		"generic/1",
		"generic/2",
		"generic/3",
		"generic/4",
		// generic/5 is above 100 countdown, so should be filtered out
	}

	if want, have := len(expectedIDs), len(results); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
	for _, expectedID := range expectedIDs {
		found := false
		for _, object := range results {
			if object.ID == expectedID {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("ID %#v not found in result objects", expectedID)
		}
	}
}

func TestObjectHasEitherID(t *testing.T) {
	criteria := engine.ObjectHasEitherID("obj/2", "obj/4")

	if want, have := false, criteria(engine.Object{ID: "obj/1"}); want != have {
		t.Errorf("expected %v, got %v", want, have)
	}
	if want, have := true, criteria(engine.Object{ID: "obj/2"}); want != have {
		t.Errorf("expected %v, got %v", want, have)
	}
	if want, have := false, criteria(engine.Object{ID: "obj/3"}); want != have {
		t.Errorf("expected %v, got %v", want, have)
	}
	if want, have := true, criteria(engine.Object{ID: "obj/4"}); want != have {
		t.Errorf("expected %v, got %v", want, have)
	}
}
